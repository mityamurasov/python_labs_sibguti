def func(x):
	if (( x % 3 == 0) & (x % 5 == 0)):
		return "foo bar"
	elif ( x % 3 == 0):
		return "foo"
	elif ( x % 5 == 0):
		return "bar"
	else:
		return x

if __name__ == '__main__':
	assert func(15) == "foo bar"
	assert func(25) == "bar"
	assert func(24) == "foo"
	assert func(17) == "17"
